use std::error::Error;
use std::path::Path;
use std::fs;
use std::io::{self, Write};

use rand::Rng;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Action {
    Practice,
    Search,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Direction {
    LeftRight,
    RightLeft
}

#[derive(Debug, Clone, PartialEq)]
pub struct FlashCard {
    left: Vec<String>,
    right: Vec<String>,
}

impl FlashCard {
    pub fn new(left: Vec<String>, right: Vec<String>) -> Self {
        Self { left, right }
    }

    pub fn matches(&self, term: String, direction: Direction) -> bool {
        match direction {
            Direction::LeftRight => self.left.contains(&term),
            Direction::RightLeft => self.right.contains(&term),
        }
    }

    pub fn print(&self) {
        println!("{} : {}", self.left.join(", "), self.right.join(", "));
    }
}

pub fn parse_file<P: AsRef<Path>>(filename: P) -> Result<Vec<FlashCard>, Box<dyn Error>> {
    let contents = fs::read_to_string(filename)?;
    let mut cards = Vec::new();
    let mut leftright_separator = String::new();
    let mut entry_separator = String::new();
    for (idx, line) in contents.lines().enumerate() {
        if idx == 0 {
            let split = line.split_whitespace().collect::<Vec<_>>();
            if split.len() != 2 {
                Err("Incorrect amount of separator tokens on line 1 (should have 2)")?;
            }
            leftright_separator = split[0].to_string();
            entry_separator = split[1].to_string();
            continue;
        }
        if line.is_empty() {continue;}
        let left_right = line.split(&leftright_separator).collect::<Vec<_>>();
        if left_right.len() != 2 {
            Err(format!("Incorrect amount of left-right separator tokens on line {} (should have 1 left-right separator per line)", idx + 1))?;
        }
        let left = left_right[0].split(&entry_separator).map(|e| e.trim().to_string()).collect::<Vec<_>>();
        let right = left_right[1].split(&entry_separator).map(|e| e.trim().to_string()).collect::<Vec<_>>();
        cards.push(FlashCard::new(left, right));
    }
    Ok(cards)
}

pub fn search_terms(terms: &Vec<FlashCard>, search: String, direction: Direction) -> Vec<FlashCard> {
    let mut results = Vec::new();

    for term in terms {
        let search_str = match direction {
            Direction::LeftRight => { term.left.join(" ") },
            Direction::RightLeft => { term.right.join(" ") },
        };

        if search_str.contains(&search) {
            results.push(term.clone());
        }
    }

    results
}

pub fn run(filenames: Vec<String>, action: Action, direction: Direction) -> Result<(), Box<dyn Error>> {
    let mut terms = Vec::new();
    for filename in filenames {
        terms.append(&mut parse_file(filename)?);
    }

    match action {
        Action::Practice => {
            let mut rng = rand::thread_rng();
            let opposite_direction = if direction == Direction::LeftRight {Direction::RightLeft} else {Direction::LeftRight};
            loop {
                let term = &terms[rng.gen_range(0..terms.len())];
                match direction {
                    Direction::LeftRight => { println!("{}", term.left.join(", ")); },
                    Direction::RightLeft => { println!("{}", term.right.join(", ")); },
                }
                print!("Answer: ");
                io::stdout().flush()?;
                let mut answer = String::new();
                io::stdin().read_line(&mut answer)?;
                if term.matches(answer.trim().to_string(), opposite_direction) {
                    println!("Correct!");
                } else {
                    println!("Incorrect!");
                }
                term.print();
                println!("\n\n");
            }
        },
        Action::Search => {
            let mut search = String::new();
            print!("Search: ");
            io::stdout().flush()?;
            io::stdin().read_line(&mut search)?;
            for term in search_terms(&terms, search.trim().to_string(), direction) {
                term.print();
            }
        },
    }
    Ok(())
}
