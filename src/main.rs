use std::process;

use rargsxd::*;

fn main() {
    let mut args = ArgParser::from_argv0();
    args.author("BubbyRoosh")
        .version("0.1.0")
        .info("Flash card program that allows two-way term search")
        .copyright("Copyright (c) 2021 BubbyRoosh")
        .usage("{name} [args] term_file ...")
        .args(vec!(
            Arg::str("action", "practice")
                .long("action")
                .short('a')
                .help("What flsh should do. Defaults to \"practice\""),
            Arg::str("direction", "left-right")
                .long("direction")
                .short('d')
                .help("The direction for searching or practice. Defaults to \"left-right\""),
        ))
        .parse();

    let action = match args.get_str("action").as_str() {
        "practice" => flsh::Action::Practice,
        "search" => flsh::Action::Search,
        _ => {
            eprintln!("{} is not a valid action. Valid actions are \"practice\" and \"search\"", args.get_str("action"));
            process::exit(1);
        },
    };

    let direction = match args.get_str("direction").as_str() {
        "left-right" => flsh::Direction::LeftRight,
        "right-left" => flsh::Direction::RightLeft,
        _ => {
            eprintln!("{} is not a valid direction. Valid directions are \"left-right\" and \"right-left\"", args.get_str("direction"));
            process::exit(1);
        },
    };

    if args.extra.len() == 0 {
        eprintln!("No file names for terms given");
        process::exit(1);
    }
    if let Err(e) = flsh::run(args.extra, action, direction) {
        eprintln!("Error running flsh: {}", e);
        process::exit(1);
    }
}
