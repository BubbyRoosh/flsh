# flsh

Simple (and ugly) flashcard program allowing for a super simple file format.

## Installation

```sh
# Clone the repo
git clone https://codeberg.org/BubbyRoosh/flsh
# Install using cargo
cargo install --path ./flsh
```

## Usage

Command line arguments can be seen by passing "--help"

* --action can have either "practice" (default) or "search"
* --direction can have either "left-right" (default) or "right-left"

## File format

A file just needs to have the first line with 2 tokens, separated by a space.
These tokens can be of any length as long as they are UTF-8.

The first token is for separating the "left half" and the "right half" of each term.
The second token is for separating different entries within each term, as some words may have multiple meanings.

The file [tokipona](./tokipona) shows an example of this format.
